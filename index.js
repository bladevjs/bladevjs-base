
var express = require('express');
var app = express();
var path = require('path');

app.get('/', function(req, res) {
 res.sendFile(path.join(__dirname + '/index.html'));
});

 app.use("/", express.static( __dirname ));
 app.use(function(req, res, next) {
  res.status(404).sendFile(path.join(__dirname + '/error.html'));
});


app.listen(8081, function() {
  console.log('Aplicación ejemplo, escuchando el puerto 8081!');
});