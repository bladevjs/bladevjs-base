import {mlInitAPP}  from '../node_modules/bladevjs/dist/starter/bladevjs-starter.js';
var ml_js_files = ["node_modules/bladevjs/dist/min/bladev.min.js", 
                   "config-app/services/endpoints.js",
                   "config-app/language/en/messages.js",
                   "config-app/validations/customValidations.js",
                   "../controllers/CatalogsController.js"
                  ];
mlInitAPP(ml_js_files, "1.0", "en");