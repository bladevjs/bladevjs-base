
function mlPostalCode(element) {
	var re = /((^[5]{1})[0-2][0-9]{3})|((^[0]{1})[1-9]{1}[0-9]{3})|((^[1-4]{1})[0-9]{1}[0-9]{3})$/;
	return re.test($(element)[0].val());
}